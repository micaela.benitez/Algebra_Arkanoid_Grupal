#include "collisions.h"

using namespace gameplay;
using namespace arkanoid;

bool collisionCircleRec(Vector2 ballCenter, float ballRadius, Rectangle paddle)
{
    Vector2 point = { 0, 0 };   // Punto del perimetro del paddle mas cercano a la pelota.
    float distance = 0.f;

    point.x = ballCenter.x;  
    if (point.x < paddle.x) point.x = paddle.x;
    if (point.x > paddle.x + paddle.width) point.x = paddle.x + paddle.width;
    point.y = ballCenter.y;
    if (point.y < paddle.y) point.y = paddle.y;
    if (point.y > paddle.y + paddle.height) point.y = paddle.y + paddle.height;

    distance = sqrtf ( (ballCenter.x - point.x)*(ballCenter.x - point.x) + (ballCenter.y - point.y)*(ballCenter.y - point.y) );

    if (distance < ballRadius) return true;
    else return false;
}

void collisionBallPaddle()
{
    float hypotenuse = 0;
    float barDeviation = 0;
    float initialAngleDirection = 0;
    float finalAngleDirection = 0;
    float minAngle = 35;

    if (collisionCircleRec(ball.position, ball.radius, { player.position.x - player.size.x / 2, player.position.y - player.size.y / 2, player.size.x, player.size.y }))
    {
        if (ball.speed.y > 0)   // Para que solo choque cuando la pelota va hacia abajo
        {
            // ----- REBOTE NORMAL -----
            hypotenuse = sqrtf(powf(ball.speed.y, 2) + powf(ball.speed.x, 2));   // Se vuelve a calcular porque es afectada por la gravedad

            initialAngleDirection = fabsf(asinf(ball.speed.y / hypotenuse) * 180 / PI);   // Se calcula el �ngulo con el que viene la pelota

            // Angulo final: depende su signo de la direccion de la pelota
            if (ball.speed.x < 0) finalAngleDirection = -fabsf(initialAngleDirection);
            else if (ball.speed.x > 0) finalAngleDirection = fabsf(initialAngleDirection);

            // ----- VELOCIDAD ----- 
            ball.speed.x = (cosf(finalAngleDirection * PI / 180) * hypotenuse);   // Los (* 180 / PI) son porque math.h trabaja con radianes
            ball.speed.y = -fabsf(sinf(finalAngleDirection * PI / 180) * hypotenuse);

            if (finalAngleDirection < 0) ball.speed.x *= -1;

            // ----- SUMA DE MODIFICACI�N DE BARRA -----
            barDeviation = (maxAngle * (ball.position.x - player.position.x)) / (player.size.x / 2);   // �ngulo que modifica donde impacte

            if (barDeviation > maxAngle) barDeviation = maxAngle;
            else if (barDeviation < -maxAngle) barDeviation = -maxAngle;

            // ----- CHEQUEOS ----- 
            if (ball.position.x < player.position.x)   // Golpe en posicion de la Pelota con respecto a la Barra
            {
                if (ball.speed.x > 0) barDeviation = -barDeviation;

                finalAngleDirection += barDeviation;
                if (finalAngleDirection < minAngle && ball.speed.x < 0) finalAngleDirection = -minAngle;
            }
            else
            {
                finalAngleDirection -= barDeviation;
                if (finalAngleDirection < minAngle && ball.speed.x > 0) finalAngleDirection = minAngle;
            }

            {
                if (finalAngleDirection < minAngle && finalAngleDirection > 0)
                    finalAngleDirection = minAngle;
                else if (finalAngleDirection > -minAngle && finalAngleDirection < 0)
                    finalAngleDirection = -minAngle;
            }

            ball.speed.y = -fabsf(sinf(finalAngleDirection * PI / 180) * hypotenuse);
            ball.speed.x = (cosf(finalAngleDirection * PI / 180) * hypotenuse);

            if (finalAngleDirection < 0) ball.speed.x *= -1;
        }
    }
}

void collisionBallBricks()
{
    for (int i = 0; i < linesOfBricks; i++)
    {
        for (int j = 0; j < bricksPerLine; j++)
        {
            if (brick[i][j].active)
            {
                // Hit below
                if (((ball.position.y - ball.radius) <= (brick[i][j].position.y + brickSize.y / 2)) &&
                    ((ball.position.y - ball.radius) > (brick[i][j].position.y + brickSize.y / 2 + ball.speed.y)) &&
                    ((fabsf(ball.position.x - brick[i][j].position.x)) < (brickSize.x / 2 + ball.radius * 2 / 3)) && (ball.speed.y < 0))
                {
                    brick[i][j].active = false;
                    ball.speed.y *= -1;
                }
                // Hit above
                else if (((ball.position.y + ball.radius) >= (brick[i][j].position.y - brickSize.y / 2)) &&
                    ((ball.position.y + ball.radius) < (brick[i][j].position.y - brickSize.y / 2 + ball.speed.y)) &&
                    ((fabsf(ball.position.x - brick[i][j].position.x)) < (brickSize.x / 2 + ball.radius * 2 / 3)) && (ball.speed.y > 0))
                {
                    brick[i][j].active = false;
                    ball.speed.y *= -1;
                }
                // Hit left
                else if (((ball.position.x + ball.radius) >= (brick[i][j].position.x - brickSize.x / 2)) &&
                    ((ball.position.x + ball.radius) < (brick[i][j].position.x - brickSize.x / 2 + ball.speed.x)) &&
                    ((fabsf(ball.position.y - brick[i][j].position.y)) < (brickSize.y / 2 + ball.radius * 2 / 3)) && (ball.speed.x > 0))
                {
                    brick[i][j].active = false;
                    ball.speed.x *= -1;
                }
                // Hit right
                else if (((ball.position.x - ball.radius) <= (brick[i][j].position.x + brickSize.x / 2)) &&
                    ((ball.position.x - ball.radius) > (brick[i][j].position.x + brickSize.x / 2 + ball.speed.x)) &&
                    ((fabsf(ball.position.y - brick[i][j].position.y)) < (brickSize.y / 2 + ball.radius * 2 / 3)) && (ball.speed.x < 0))
                {
                    brick[i][j].active = false;
                    ball.speed.x *= -1;
                }
            }
        }
    }
}