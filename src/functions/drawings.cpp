#include "functions/drawings.h"

using namespace gameplay;
using namespace arkanoid;

void drawPaddle()
{
	DrawRectangle(static_cast<int>(player.position.x - player.size.x / 2), static_cast<int>(player.position.y - player.size.y / 2), static_cast<int>(player.size.x), static_cast<int>(player.size.y), BLACK);
}

void drawLives()
{
	for (int i = 0; i < player.lifes; i++) DrawRectangle(20 + 40 * i, screenHeight - 30, 35, 10, LIGHTGRAY);
}

void drawBall()
{
	DrawCircleV(ball.position, ball.radius, MAROON);
	DrawLine(static_cast<int>(ball.position.x), static_cast<int>(ball.position.y), static_cast<int>(ball.position.x + ball.speed.x * 100), static_cast<int>(ball.position.y + ball.speed.y * 100), GREEN);
}

void drawBricks()
{
    for (int i = 0; i < LINES_OF_BRICKS; i++)
    {
        for (int j = 0; j < BRICKS_PER_LINE; j++)
        {
            if (brick[i][j].active)
            {
                if ((i + j) % 2 == 0) DrawRectangle(static_cast<int>(brick[i][j].position.x - brickSize.x / 2), static_cast<int>(brick[i][j].position.y - brickSize.y / 2), static_cast<int>(brickSize.x), static_cast<int>(brickSize.y), GRAY);
                else DrawRectangle(static_cast<int>(brick[i][j].position.x - brickSize.x / 2), static_cast<int>(brick[i][j].position.y - brickSize.y / 2), static_cast<int>(brickSize.x), static_cast<int>(brickSize.y), DARKGRAY);
            }
        }
    }
}

void drawScreenTexts()
{
    if (pause) DrawText("GAME PAUSED", screenWidth / 2 - MeasureText("GAME PAUSED", 40) / 2, screenHeight / 2 - 40, 40, BLACK);
    if (gameOver) DrawText("PRESS [ENTER] TO PLAY AGAIN", GetScreenWidth() / 2 - MeasureText("PRESS [ENTER] TO PLAY AGAIN", 20) / 2, GetScreenHeight() / 2 - 50, 20, GRAY);
}