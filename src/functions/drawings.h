#ifndef DRAWINGS_H
#define DRAWINGS_H

#include "raylib.h"
#include "game/gameplay.h"

void drawPaddle();
void drawLives();
void drawBall();
void drawBricks();
void drawScreenTexts();

#endif