#ifndef COLLISIONS_H
#define COLLISIONS_H

#include "raylib.h"
#include "game/gameplay.h"

void collisionBallPaddle();
void collisionBallBricks();

#endif