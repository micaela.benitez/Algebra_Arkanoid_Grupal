#ifndef GAMEPLAY_H
#define GAMEPLAY_H

#include "raylib.h"

#include <math.h>

#include "screen.h"
#include "functions/collisions.h"
#include "functions/drawings.h"

const int LINES_OF_BRICKS = 5;
const int BRICKS_PER_LINE = 20;

namespace gameplay
{
    namespace arkanoid
    {
        extern int maxLives;
        extern int linesOfBricks;
        extern int bricksPerLine;
        extern int gravity;

        typedef struct Player {
            Vector2 position;
            Vector2 size;
            int lifes;
        } Player;

        typedef struct Ball {
            Vector2 position;
            Vector2 speed;
            float radius;
            bool active;
        } Ball;

        typedef struct Brick {
            Vector2 position;
            bool active;
        } Brick;

        extern bool gameOver;
        extern bool pause;

        extern Player player;
        extern Ball ball;
        extern Brick brick[LINES_OF_BRICKS][BRICKS_PER_LINE];
        extern Vector2 brickSize;

        extern float maxAngle;

        void playGame();

        static void initGame();
        static void updateGame();
        static void drawGame();
    }    
}

#endif