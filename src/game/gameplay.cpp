#include "gameplay.h"

namespace gameplay
{
    namespace arkanoid
    {
        int maxLives = 5;
        int linesOfBricks = LINES_OF_BRICKS;
        int bricksPerLine = BRICKS_PER_LINE;
        int gravity = 0.005f;

        bool gameOver = false;
        bool pause = false;

        Player player = { 0 };
        Ball ball = { 0 };
        Brick brick[LINES_OF_BRICKS][BRICKS_PER_LINE] = { 0 };
        Vector2 brickSize = { 0 };

        float maxAngle = 55;

        void playGame()
        {
            initGame();
            while (!WindowShouldClose())
            {
                updateGame();
                drawGame();
            }
            CloseWindow();
        }

        static void initGame()
        {
            InitWindow(screenWidth, screenHeight, "Arkanoid");
            SetTargetFPS(60);

            const float screenMeasurement = 7 / 8.f;
            const short playerHeight = 20;
            const int bricksHeight = 40;
            int initialDownPosition = 50;

            brickSize = { (float)(GetScreenWidth() / bricksPerLine), (float)bricksHeight };

            player.position = { screenWidth / 2, screenHeight * screenMeasurement };
            player.size = { screenWidth / 10 , playerHeight };
            player.lifes = maxLives;

            ball.position = { screenWidth / 2, screenHeight * screenMeasurement - playerHeight };
            ball.speed = { 0, 0 };
            ball.radius = 7;
            ball.active = false;

            for (int i = 0; i < linesOfBricks; i++)
            {
                for (int j = 0; j < bricksPerLine; j++)
                {
                    brick[i][j].position = { j * brickSize.x + brickSize.x / 2, i * brickSize.y + initialDownPosition };
                    brick[i][j].active = true;
                }
            }
        }

        static void updateGame()
        {
            const int playerSpeed = 5;
            const float ballSpeedX = 1.5f;
            const int ballSpeedY = -2;

            if (!gameOver)
            {
                if (IsKeyPressed('P')) pause = !pause;

                if (!pause)
                {
                    // Paddle
                    if (IsKeyDown(KEY_LEFT)) player.position.x -= playerSpeed;
                    if ((player.position.x - player.size.x / 2) <= 0) player.position.x = player.size.x / 2;
                    if (IsKeyDown(KEY_RIGHT)) player.position.x += playerSpeed;
                    if ((player.position.x + player.size.x / 2) >= screenWidth) player.position.x = screenWidth - player.size.x / 2;

                    // Ball
                    if (!ball.active && IsKeyPressed(KEY_SPACE))
                    {
                        ball.active = true;
                        ball.speed.x = ballSpeedX;
                        ball.speed.y = ballSpeedY;
                    }

                    if (ball.active)
                    {
                        ball.speed.y += gravity;

                        ball.position.x += ball.speed.x;
                        ball.position.y += ball.speed.y;
                    }
                    else
                    {
                        ball.position = { player.position.x, screenHeight * 7 / 8 - player.size.y };
                    }

                    // If the ball is lost
                    if (((ball.position.x + ball.radius) >= screenWidth) || ((ball.position.x - ball.radius) <= 0)) ball.speed.x *= -1;
                    if ((ball.position.y - ball.radius) <= 0) ball.speed.y *= -1;
                    if ((ball.position.y + ball.radius) >= screenHeight)
                    {
                        ball.speed = { 0, 0 };
                        ball.active = false;

                        player.lifes--;
                    }

                    // Collisions
                    collisionBallPaddle();
                    collisionBallBricks();

                    // Game over check
                    if (player.lifes <= 0) gameOver = true;
                    else
                    {
                        gameOver = true;

                        for (int i = 0; i < linesOfBricks; i++)
                        {
                            for (int j = 0; j < bricksPerLine; j++)
                            {
                                if (brick[i][j].active) gameOver = false;
                            }
                        }
                    }
                }
            }
            else
            {
                // Play again
                if (IsKeyPressed(KEY_ENTER))
                {
                    initGame();
                    gameOver = false;
                }
            }
        }

        static void drawGame()
        {
            BeginDrawing();
            ClearBackground(RAYWHITE);

            if (!gameOver)
            {
                drawPaddle();
                drawLives();
                drawBall();
                drawBricks();               
            }
            drawScreenTexts();

            EndDrawing();
        }
    }
}